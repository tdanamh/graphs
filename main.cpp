#include <iostream>
#include <fstream>
#include <queue>

using namespace std;
ifstream f("date.in");

struct muchie {
    int nod1, nod2, cost;
};

class Graf {
    int a[100][100];
    int n;
    int vizitat[100];
    int dim = 100;
    queue<int> coada;
    queue<int> coada2;

public:
    Graf() {
        for(int i=0;i<100;i++) {
            for(int j=0; j<100; j++) {
                a[i][j] = 0;
            }
        }
        resetare_vizitat();
    }
    void resetare_vizitat() {
        for(int i = 0;i < 100; i++) {
            vizitat[i] = 0;
        }
    }
    void construire() {
        f>>n;
        int m;
        f>>m;
        int x,y,z;
        for(int i=0; i < m ;i++) {
            f>>x>>y>>z;
            a[x][y] = z;
            a[y][x] = z;
        }
    }
    void DFS(int start) {
        cout<<start<<" ";
        vizitat[start] = 1;
        for(int i=0;i < n;i++) {    ///ia pe rand toti vecinii
            if(a[i][start] != 0 && vizitat[i] == 0) {   ///nod start vecin cu i
                DFS(i);
            }
        }
    }
    void BFS(int start) {   ///ia toti vecinii fiecarui nod si reia
        vizitat[start] = 1;
        coada.push(start);
        while( !coada.empty()) {    ///cat timp coada nu e vida
            int elem = coada.front();
            cout<<elem<<" ";
            coada.pop();
            for(int i=0; i<n; i++) {
                if(a[i][elem] != 0 && vizitat[i] == 0) {
                    coada.push(i);
                    vizitat[i] = 1;
                }
            }
        }
    }
    void kruskal(){
        vector<muchie> v;
        for(int i = 1; i <= n; i++){
            for(int j = i; j <= n; j++){
                if(a[i][j] != 0){
                    muchie temp;
                    temp.nod1 = i;
                    temp.nod2 = j;
                    temp.cost = a[i][j];
                    v.push_back(temp);
                }
            }
        }
        for(int k = 0; k < v.size() - 1; k++){
            for(int l = k + 1; l < v.size(); l++){
                if(v[k].cost > v[l].cost){
                    swap(v[k],v[l]);
                }
            }
        }
    }

    int gasireDrum(int sursa,int destinatie ) {
        vizitat[sursa]=1;
        coada2.push(sursa);
        while( !coada2.empty()) {
            int element = coada2.front();
            cout<<element<<endl;

            if(element == destinatie) return 1;

            coada2.pop();
            for(int i = 0; i < n ; i++) {
                if(a[element][i] != 0 && vizitat[i] == 0) {
                    vizitat[i] = 1;
                    coada2.push(i);
                }
            }
        }
        return 0;
    }
};

int main()
{
   Graf g;

   g.construire();
   g.DFS(0);
   cout<<endl;

   g.resetare_vizitat();
   g.BFS(0);
   cout<<endl;
    g.resetare_vizitat();
   cout<<g.gasireDrum(3,2);
    return 0;
}
